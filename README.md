# Myper Sac Functional Test
Proyecto backend hecho en .Net Core, Entity Framework, y SqlServer, para el registro de trabajadores en una base de datos.

##### Base de datos utilizada: TrabajadoresPrueba
![imagen](/uploads/bafe43dbc1cc627bfb28baefb206c0fe/imagen.png)

### Configuración del Proyecto
Para lograr inicializar el proyecto correctamente debemos realizar una serie de configuraciones.

Descarga y ejecución del script de la base de datos:<br>
- Link del script :point_right: [Click para ir al archivo :link:](https://gitlab.com/mypersac-functionaltest/mypersac-trabajadoresbackend/-/blob/main/Base%20de%20datos%20Trabajadores%20Data.sql): 
- Link del script de los procedimientos almacenados y algunas consultas :point_right: [Click para ir al archivo](https://gitlab.com/mypersac-functionaltest/mypersac-trabajadoresbackend/-/blob/main/Extenci%C3%B3n%20Base%20de%20datos%20Trabajadores%20Data.sql) :link:

:eyes: Es necesario tener los procedimientos almacenados dentro de la base de datos para que la API funcione correctamente.

### Configuraciones en con Visual Studio
Debemos configurar ciertas cosas en cuanto a código para que la aplicación funcione correctamente:

> #### Concideraciones a tomar en caso de hacer Scaffolding
> Para mapear las tablas de la base de datos debemos ejecutar el siguiente comando reemplazando "<texto>" por tus datos.
>
> ```Bash
> Scaffold-DbContext "Server=<tuservidor>; Database=TrabajadoresPrueba; Trusted_Connection=True; Encrypt=False;" Microsoft.EntityFrameworkCore.SqlServer OutputDir Models -force
>```
>
> Al hacer el Scaffolding de la base de datos perderemos algunas configuraciones, por lo que te mostraré cuales son y lo que debes hacer para recuperarlas.
> 1. Dirigirse a la carpeta models y modificar el archivo "TrabajadoresPruebaContext.cs" y añadir la siguiente línea de código debajo de la línea 19:
> ```csharp
> public DbSet<SpTrabajadorResponse> SpTrabajadorResponses { get; set; }
> ````
> ![imagen](/uploads/cae4e1ee40b9041dd470711eed4c7ee0/imagen.png)
> *Imagen del código insertado*

<br>

> #### Configuración de "appsettings.json"
> Para configurar tu propia candena de conexión debemos modificar el archivo "appsettings.json" e insertar nuestra cadena de la siguiente forma:
> <br><br>
> ![imagen](/uploads/f5e0c1bcb0d97614d013a32025ca956d/imagen.png)
> *Imagen del archivo con la la configuración realizada*
> <br><br>
> Esta configuración es mas útil cuando no tienes la necesidad de ejecutar el Scaffolding y solo quieres iniciar la aplicación con tu propia cadena de conexión.

<br>

> #### Configuración de CORS
> Esta configuaración es importante cuando queramos permitir a ciertos el fetching de datos desde diferentes orígenes. Mi configuración esta hecha para consumirla con una aplicación hecha en Reactjs :point_right: [Link del repositorio](https://gitlab.com/mypersac-functionaltest/mypersac-trabajadoresfrontend) desde un entorno local.<br>
> Para realizar esta configuración debes agrebar los orígenes que necesitos dentro del método "WithOrigins" el cual acepta multiples parámetros de tipo 'string'.<br><br>
> ![imagen](/uploads/d47ef528de3e5daac64ea38c7a434c67/imagen.png)
> *Imagen demostrativa de la configuración de CORS*

Estos serían los pasos a seguir o a tomar en cuenta para el correcto funcionamiento de la aplicación.<br>
### Gracias!! 🤗
