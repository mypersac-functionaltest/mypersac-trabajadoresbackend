
/*Procedimientos almacenados*/

/*Listado de trabajadores*/
CREATE PROCEDURE spListarTrabajadores
AS
BEGIN
	SELECT
		t.[Id],
		t.[TipoDocumento],
		t.[NumeroDocumento],
		t.[Nombres],
		t.[Sexo],
		dep.[NombreDepartamento],
		prov.[NombreProvincia],
		dis.[NombreDistrito]
	FROM [Trabajadores] t
	INNER JOIN [Departamento] dep ON dep.Id = t.IdDepartamento
	INNER JOIN [Provincia] prov ON prov.Id = t.IdProvincia
	INNER JOIN [Distrito] dis ON dis.Id = t.IdDistrito
END
GO

EXEC dbo.spListarTrabajadores
GO

DROP PROCEDURE dbo.spListarTrabajadores
GO
/*Obteniendo información de trabajadores por Sexo*/
CREATE PROCEDURE spListarTrabajadoresPorSexo
	@sexo varchar(1)
AS
BEGIN
	SELECT
		t.[Id],
		t.[TipoDocumento],
		t.[NumeroDocumento],
		t.[Nombres],
		t.[Sexo],
		dep.[NombreDepartamento],
		prov.[NombreProvincia],
		dis.[NombreDistrito]
	FROM [Trabajadores] t
	INNER JOIN [Departamento] dep ON dep.Id = t.IdDepartamento
	INNER JOIN [Provincia] prov ON prov.Id = t.IdProvincia
	INNER JOIN [Distrito] dis ON dis.Id = t.IdDistrito
	WHERE t.[sexo] = @sexo
END
GO

EXEC dbo.spListarTrabajadoresPorSexo 'F'
GO

DROP PROCEDURE dbo.spListarTrabajadores
GO

/*Obteniendo información de un trabajador mediante su Id*/
CREATE PROCEDURE spObtenerTrabajador
	@Id int
AS
BEGIN
	SELECT
		t.[Id],
		t.[TipoDocumento],
		t.[NumeroDocumento],
		t.[Nombres],
		t.[Sexo],
		dep.[NombreDepartamento],
		prov.[NombreProvincia],
		dis.[NombreDistrito]
	FROM [Trabajadores] t
	INNER JOIN [Departamento] dep ON dep.Id = t.IdDepartamento
	INNER JOIN [Provincia] prov ON prov.Id = t.IdProvincia
	INNER JOIN [Distrito] dis ON dis.Id = t.IdDistrito
	Where t.[Id] = @Id
	ORDER BY t.[Id]
END
GO

EXEC dbo.spObtenerTrabajador 1
GO

DROP procedure dbo.spObtenerTrabajador
GO

/*-------------------------------------------------*/

/*Buscando provincias por IdDepartamento*/
SELECT
	p.[Id],
	p.[NombreProvincia],
	d.[NombreDepartamento],
	d.[Id]
FROM [Provincia] p 
INNER JOIN [Departamento] d ON d.[Id] = p.[IdDepartamento]
WHERE p.[IdDepartamento] = 1
GO

/*Buscando Distrino por IdProvincia*/
SELECT
	d.[Id],
	d.[NombreDistrito],
	p.[NombreProvincia],
	p.[Id]
FROM [Distrito] d
INNER JOIN [Provincia] p ON p.Id = d.IdProvincia
WHERE d.IdProvincia = 1
GO

/*Comprobando si la provincia indicada pertenece al departamento*/
SELECT
	d.NombreDepartamento,
	p.NombreProvincia
FROM [Provincia] p
INNER JOIN [Departamento] d ON d.Id = p.IdDepartamento
WHERE d.Id = 15 AND p.Id = 128

/*Comprobando si el distrito pertenece a la provincia*/
SELECT
	d.NombreDistrito,
	p.NombreProvincia
FROM [Distrito] d
INNER JOIN [Provincia] p ON p.Id = d.IdProvincia
WHERE p.Id = 1 AND d.Id = 2681

/*Calculando cantidad de departamentos*/
SELECT
	COUNT(d.[Id]) as [Cantidad de departamentos]
FROM [Departamento] d


/*Calculando cantidad de provincias*/
SELECT
	COUNT(p.[Id]) as [Cantidad de provincias]
FROM [Provincia] p

/*Calculando cantidad de distritos*/
SELECT
	COUNT(d.[Id]) as [Cantidad de distritos]
FROM [Distrito] d